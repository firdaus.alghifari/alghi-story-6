from django.test import TestCase, Client
from django.urls import resolve

from .views import index

class IndexTest(TestCase):
    def test_index_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_view(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home/index.html')

    def test_form_exists(self):
        response = Client().get('/')
        self.assertIn('</form>', response.content.decode())

    def test_submit_status(self):
        name = 'Yolo Cat'
        message = 'Heyyyy!'
        response = Client().post('/', {
            'name' : name,
            'message' : message
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn(name, response.content.decode())
        self.assertIn(message, response.content.decode())


# SELENIUM TEST #
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

class IndexLiveTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        self.selenium = webdriver.Chrome(chrome_options = chrome_options)
        super(IndexLiveTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(IndexLiveTest, self).tearDown()

    def test_submitForm(self):
        selenium = self.selenium

        selenium.get('http://127.0.0.1:8000/')
        time.sleep(1)

        name = selenium.find_element_by_id('id_name')
        message = selenium.find_element_by_id('id_message')
        submit = selenium.find_element_by_id('id_submit')

        name.send_keys('Kocheng')
        message.send_keys('Coba Coba')

        submit.send_keys(Keys.RETURN)
        time.sleep(1)

        assert 'Kocheng' in selenium.page_source
        assert 'Coba Coba' in selenium.page_source
