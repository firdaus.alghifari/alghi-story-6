from django.shortcuts import render

from .forms import StatusForm
from .models import Status

# Create your views here.

def index(request):
    if request.method == 'POST':
        status_form = StatusForm(request.POST)
        if status_form.is_valid():
            status_form.save()
    status_form = StatusForm()
    list_status = Status.objects.all()
    return render(request, 'home/index.html', {
        'status_form': status_form,
        'list_status': list_status
    })
