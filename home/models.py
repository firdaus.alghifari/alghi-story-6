from django.db import models

from datetime import datetime

# Create your models here.
class Status(models.Model):
    name = models.CharField(max_length = 100)
    message = models.CharField(max_length = 300)
    posted_date = models.DateTimeField(default=datetime.now, blank=True)
